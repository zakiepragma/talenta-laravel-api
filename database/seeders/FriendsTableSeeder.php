<?php

namespace Database\Seeders;

use App\Models\Friend;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Friend::create([
            'name' => 'Muhammad Zakie',
            'gender' => 'male',
            'age' => 42,
        ]);

        Friend::create([
            'name' => 'Renisa Nurayu Nastiti',
            'gender' => 'female',
            'age' => 38,
        ]);

        Friend::create([
            'name' => 'Al-Muzani',
            'gender' => 'male',
            'age' => 17,
        ]);

        Friend::create([
            'name' => 'El-Owais',
            'gender' => 'male',
            'age' => 17,
        ]);

        Friend::create([
            'name' => 'Aisyah',
            'gender' => 'female',
            'age' => 11,
        ]);
    }
}
