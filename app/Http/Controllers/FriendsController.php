<?php

namespace App\Http\Controllers;

use App\Models\Friend;
use Illuminate\Http\Request;

class FriendsController extends Controller
{
    public function index()
    {
        $friends = Friend::all();
        return response()->json($friends);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'gender' => 'required|in:male,female',
            'age' => 'required|integer|min:1',
        ]);

        $friend = Friend::create($validatedData);
        return response()->json($friend, 201);
    }

    public function show(Friend $friend)
    {
        return response()->json($friend);
    }

    public function update(Request $request, Friend $friend)
    {
        $validatedData = $request->validate([
            'name' => 'sometimes|required|string|max:255',
            'gender' => 'sometimes|required|in:male,female',
            'age' => 'sometimes|required|integer|min:1',
        ]);

        $friend->update($validatedData);
        return response()->json($friend);
    }

    public function destroy(Friend $friend)
    {
        $friend->delete();
        return response()->json(null, 204);
    }

    public function getFriendPercentage()
    {
        $male_count = Friend::where('gender', 'male')->count();
        $female_count = Friend::where('gender', 'female')->count();
        $total_gender_count = $male_count + $female_count;

        $male_percentage = ($male_count / $total_gender_count) * 100;
        $female_percentage = ($female_count / $total_gender_count) * 100;


        $under_20_count = Friend::where('age', '<=', 19)->count();
        $above_20_count = Friend::where('age', '>=', 20)->count();
        $total_age_count = $under_20_count + $above_20_count;

        $under_20_percentage = ($under_20_count / $total_age_count) * 100;
        $above_20_percentage = ($above_20_count / $total_age_count) * 100;

        return response()->json([
            'male_percentage' => $male_percentage,
            'female_percentage' => $female_percentage,
            'under_20_percentage' => $under_20_percentage,
            'above_20_percentage' => $above_20_percentage,
        ]);
    }
}
